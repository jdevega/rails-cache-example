# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
require 'faker'
require 'pry'

users = []
microposts = []

50.times do |i|
  i += 1
  users << {email: "user#{i}@test.com", name: "user#{i}", password: 'redradix', password_confirmation: 'redradix'}
end

User.create(users)
users = User.all

users.each do |u|
  20.times do
    user = users.sample
    u.followers << user unless user == u || u.followers.include?(user)
  end
  200.times { microposts << {user_id: u.id, content: Faker::Lorem.sentence(20)} }
end
Micropost.create(microposts)
